@extends('master')

@section('content')
  <div class="container">
    <div class="level-one-main">
      <div class="parking-div row">
        <div class="col-sm customed left">
          <ul class="list-group">
            @foreach ($parking_first as $pf)
              @if ($pf->status)
                <li class="list-group-item list-group-item-danger">{{ $pf->parking_grid }}</li>
              @else
                <li class="list-group-item list-group-item-success">{{ $pf->parking_grid }}</li>
              @endif
            @endforeach
          </ul>
        </div>
        <div class="col-sm customed right">
          <ul class="list-group">
            @foreach ($parking_second as $ps)

              @if ($ps->status)
                <li class="list-group-item list-group-item-danger">{{ $ps->parking_grid }}</li>
              @else
                <li class="list-group-item list-group-item-success">{{ $ps->parking_grid }}</li>
              @endif
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
  <script>
    setTimeout(function(){
      window.location.reload(1);
    }, 10000);
  </script>
@endsection
