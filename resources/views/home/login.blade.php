@extends('master')

@section('content')
  <div class="container">
    <div class="login-main">
      <div class="title">
        <span>Park Here</span>
      </div>
      <div class="login-form">
        <form method="post" action="">
          {{ csrf_field() }}
          <div class="input-group mb-3 customed">
            <div class="input-group-prepend customed-logo">
              <span class="input-group-text">
                <i class="fas fa-user"></i>
              </span>
            </div>
              <input name="email" type="text" class="form-control" placeholder="Email Address">
          </div>
          <div class="input-group mb-3 customed">
            <div class="input-group-prepend customed-logo">
              <span class="input-group-text">
                <i class="fas fa-key"></i>
              </span>
            </div>
            <input name="password" type="password" class="form-control" placeholder="Password">
          </div>
          <div class="customed-button">
            <a href="{{ action('HomeController@register') }}" class="btn btn-info make-oval">Register</a>
            <button type="submit" class="btn btn-primary make-oval" data-toggle="modal" data-target="#loadingModal">Sign In</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
