<nav class="navbar navbar-expand-lg navbar-dark bg-dark customed-navbar">
  <a class="navbar-brand" href="/">Park Here</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav mr-auto">
      @if (url()->current() == action('HomeController@index'))
        <li class="nav-item active">
      @else
        <li class="nav-item">
      @endif
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      @if (url()->current() == action('HomeController@level_one'))
        <li class="nav-item active">
      @else
        <li class="nav-item">
      @endif
        <a class="nav-link" href="{{ action('HomeController@level_one') }}">Level 1</a>
      </li>
      @if (url()->current() == action('HomeController@level_two'))
        <li class="nav-item active">
      @else
        <li class="nav-item">
      @endif
        <a class="nav-link" href="{{ action('HomeController@level_two') }}">Level 2</a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ action('HomeController@logout') }}">Logout</a>
      </li>
    </ul>
  </div>
</nav>
