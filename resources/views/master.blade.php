<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{asset('/css/app.css')}}" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <script src="{{asset('/js/app.js')}}"></script>
    <title>Park Here</title>
  </head>
  @if (url()->current() == action('HomeController@login') || url()->current() == action('HomeController@register'))
    <body class="login-body-main">
      @yield('content')
    </body>
  @else
    <body class="home-main-background">
      @include('shared.nav')
      @yield('content')
    </body>
  @endif

</html>
