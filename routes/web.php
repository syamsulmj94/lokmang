<?php

Route::get('/', 'HomeController@index');
Route::get('/login', 'HomeController@login');
Route::post('/login', 'HomeController@checking_login');
Route::get('/level-one', 'HomeController@level_one');
Route::get('/level-two', 'HomeController@level_two');
Route::get('logout', 'HomeController@logout');
Route::get('/register', 'HomeController@register');
Route::post('/register', 'HomeController@create');
Route::get('/update-status/{parking_grid}/{status}', 'DataController@update');
