<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ParkingStatus;

class DataController extends Controller
{
    public function index() {

    }

    public function update($parking_grid, $status) {

      if(ParkingStatus::where('parking_grid', $parking_grid)->count()) {

        if(!ParkingStatus::where('parking_grid', $parking_grid)->where('status', '<>', $status)->count()) {
          $json_data = ['success' => false];

          return json_encode($json_data);
        }
        else {
          $update = ['status' => $status];
          $json_data = ['success' => true];

          ParkingStatus::where('parking_grid', $parking_grid)->update($update);

          return json_encode($json_data);
        }
      }
      else {
        $json_data = ['success' => false];

        return json_encode($json_data);
      }
    }
}
