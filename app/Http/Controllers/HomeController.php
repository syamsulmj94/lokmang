<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ParkingStatus;

class HomeController extends Controller
{
    public function index() {

      if(session('login?')) {
        return view('home.index');
      }
      else {
        return redirect()->action('HomeController@login');
      }

    }

    public function login() {

      return view('home.login');
    }

    public function checking_login(Request $request) {

      $request->validate([
        'email' => 'required',
        'password' => 'required'
      ]);

      $email = $request->input('email');
      $password = $request->input('password');


      if(User::where('email', $email)->where('password', $password)->count()) {

        session([
          'login?' => true,
          'email' => $email
        ]);

        return redirect()->action('HomeController@index');
      }
      else {
        return back()->withErrors('Wrong email or password');
      }
    }

    public function level_one() {
      $parking_first = ParkingStatus::all()->where('id', '<=', 5);
      $parking_second = ParkingStatus::all()->where('id', '<=', 10)->where('id', '>', 5);



      return view(
        'home.levelone',
        compact('parking_first', 'parking_second')
      );
    }

    public function level_two() {
      $parking_first = ParkingStatus::all()->where('id', '<=', 15)->where('id', '>', 10);
      $parking_second = ParkingStatus::all()->where('id', '<=', 20)->where('id', '>', 15);



      return view(
        'home.leveltwo',
        compact('parking_first', 'parking_second')
      );
    }

    public function register() {

      return view('home.register');
    }

    public function create(Request $request) {

      $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        'confirmed-password' => 'required',
        'name' => 'required'
      ]);

      $email = $request->input('email');
      $name = $request->input('name');

      if($request->input('password') == $request->input('confirmed-password')) {

        if(!User::where('email', $email)->count()) {

          User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
          ]);

          return redirect()->action('HomeController@loginPage');
        }
        else {
          return back()
          ->withErrors(['checking' => 'The email address already exist!'])
          ->with([
            'email' => $email,
            'name' => $name
          ]);
        }
      }
      else {
        return back()
        ->withErrors(['checking' => 'Please make sure your password are the same!'])
        ->with([
          'email' => $email,
          'name' => $name
        ]);
      }
    }

    public function logout() {
      session()->flush();

      return redirect()->action('HomeController@index');
    }

}
